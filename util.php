<?php
require_once('pdo.php');
$waitFor = usleep(500000);///500 ms

function validYear($PosOrEdu){
	$valid = 1;
	for($i = 1; $i<=9; $i++){
		if( $InstOrEdu == "P" ){
			if( ! is_numeric($_POST['year'.$i]) ){
				$valid = 0;
				break;
			}
		}
		if( $InstOrEdu == "E" ){
			if( ! is_numeric($_POST['edu_year'.$i]) ){
				$valid = 0;
				break;
			}
		}		
	}
}

function insertEducations($link, $profile_id){
  try{
    $rank=1;
    for($i = 1; $i<=9; $i++){
        if( ! isset($_POST['edu_year'.$i]) ) continue;
        if( ! isset($_POST['edu_school'.$i]) ) continue;

        $year = $_POST['edu_year'.$i];
        $school = $_POST['edu_school'.$i];

        //Lookup if the school exists
        $institution_id = false;
        $qry = "SELECT institution_id FROM Institution WHERE name=:name";
        $stmt= $link->prepare($qry);
        $stmt->execute(array(':name' => $school));
        $row = $stmt->fetch(PDO::FETCH_ASSOC);
        if( $row !== false ) $institution_id = $row['institution_id'];

        //If there was no institution, insert interface
        if( $institution_id == false ){
          unset($qry);
          $qry = "INSERT INTO Institution (name) VALUES(:name)";
          $stmt= $link->prepare($qry);
          $stmt->execute(array(':name' => $school));
          $institution_id = $link->lastInsertId();
        }

        unset($qry);
        $qry = "INSERT INTO Education (profile_id, rank, year, institution_id) VALUES( :pid, :rank, :year, :iid)";
        $stmt= $link->prepare($qry);
		$stmt->execute(array(':pid' => $profile_id,
                              ':rank' => $rank,
                              ':year' => $year,
                              ':iid' => $institution_id)
        );
        $rank++;

    }
  }catch(Exception $ex){
    echo '<h3>There was an error, please contact support</h3>';
    echo '<a href="index.php"><input type="button" id="return" value="Back"></a>';
    error_log("util.php - insertEducations, SQL error= ".$ex->getMessage());
    return;
  }
}

function insertPositions($link, $profile_id){
  try{
    $rank = 1;
    for($i=1; $i<=9; $i++) {
        if ( ! isset($_POST['year'.$i]) ) continue;
        if ( ! isset($_POST['desc'.$i]) ) continue;
        $year = $_POST['year'.$i];
        $desc = $_POST['desc'.$i];

        $stmt = $link->prepare('INSERT INTO Position
            (profile_id, rank, year, description)
        VALUES ( :pid, :rank, :year, :desc)');
        $stmt->execute(array(
            ':pid' => $profile_id,
            ':rank' => $rank,
            ':year' => $year,
            ':desc' => $desc)
        );
        $rank++;
    }
  }catch(Exception $ex){
    echo '<h3>There was an error, please contact support</h3>';
    echo '<a href="index.php"><input type="button" id="return" value="Back"></a>';
    error_log("util.php - insertPositions, SQL error= ".$ex->getMessage());
    return;
  }
}

function validatePos() {
    for($i=1; $i<=9; $i++) {
        if ( ! isset($_POST['year'.$i]) ) continue;
        if ( ! isset($_POST['desc'.$i]) ) continue;
        $year = $_POST['year'.$i];
        $desc = $_POST['desc'.$i];
        if ( strlen($year) == 0 || strlen($desc) == 0 ) {
            return "All fields are required";
        }

        if ( ! is_numeric($year) ) {
            return "Position year must be numeric";
        }
    }
    return true;
}

function flashMessages(){
  if( isset($_SESSION['error']) ){
    echo '<p style="color: red;">'.htmlentities($_SESSION['error'])."</p>\n";
    unset($_SESSION['error']);
  }
  if( isset($_SESSION['success']) ){
    echo '<p style="color: green;">'.htmlentities($_SESSION['success'])."</p>\n";
    unset($_SESSION['success']);
  }
}

function loadPos($link, $profile_id){
  $stmt = $link->prepare('SELECT * FROM Position WHERE profile_id =:prof ORDER BY rank');
  $stmt->execute(array(':prof' => $profile_id));
  $position = array();
  while ( $row = $stmt->fetch(PDO::FETCH_ASSOC) ) {
    $position[] = $row;
  }
  return $position;
}

function loadEdu($link, $profile_id){
    $stmt = $link->prepare('SELECT year,name FROM Education E
                                JOIN Institution I
                                    ON E.institution_id = I.institution_id
                                WHERE profile_id =:prof ORDER BY rank');
    $stmt->execute(array(':prof' => $profile_id));
    ///$educations = $stmt->fetch(PDO::FETCH_ASSOC);
	$education = array();
	while ( $row = $stmt->fetch(PDO::FETCH_ASSOC) ) {
		$education[] = $row;
	}
   return $education;
}

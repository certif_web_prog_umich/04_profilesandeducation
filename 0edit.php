<?
	require_once('pdo.php');
	session_start();
	if( $_SESSION['name']  == '')
		die("ACCESS DENIED");
	else{
		if( $_GET['auto_id'] !== '' ){			
			$auto_id = $_GET['auto_id'];
			unset($_SESSION['auto_id']);
			$_SESSION['auto_id'] = $auto_id;
			try{
				
				$qry="SELECT * FROM autos WHERE auto_id=:auto_id";
				$stmt = $link->prepare($qry);
				$stmt->execute(array(
					':auto_id' => $auto_id)
					);		
				
				$row = $stmt->fetch(PDO::FETCH_ASSOC);
				
				$make = htmlentities($row['make']);
				$model = htmlentities($row['model']);
				$year = $row['year'];
				$mileage = $row['mileage'];
				
			}catch(Exception $ex){
				echo '<h3>There was an error, please contact support</h3>';
				echo '<a href="logout.php"><button>Return</button></a>';
				error_log("edit.php: , SQL error= ".$ex->getMessage());
				return;
			}
		}else{
			$_SESSION['error'] = "There was an error. Please contact support";
			header("Location: index.php");
			return;
		}
		
		if ( isset($_POST['edit'])  ) {
			unset($_SESSION['model']);
			unset($_SESSION['make']);
			unset($_SESSION['year']);
			unset($_SESSION['mileage']);
			
			$auto_id = $_SESSION['auto_id'];
			unset($_SESSION['auto_id']);
			
			$model = trim($_POST['model']);
			$make = trim($_POST['make']);
			$year = trim($_POST['year']);
			$mileage = trim($_POST['mileage']);
			
			$_SESSION['model'] = $model;
			$_SESSION['make'] = $make;
			$_SESSION['year'] = $year;
			$_SESSION['mileage'] = $mileage;
			
			if( ($make == '') || ($model == '') || ($year == '') || ($mileage == '') ){
				$failure = "All fields are required";
			}elseif((!is_numeric($year)) || (!is_numeric($mileage))){
				$failure = "Mileage and year must be numeric";				
			}else{
				$qryInsert = "UPDATE autos SET
								model=:md, make=:mk, year=:yr, mileage=:mi
								WHERE auto_id=:auto_id
								";
				try{
					$stmt = $link->prepare($qryInsert);
					$stmt->execute(array(
							':auto_id' => $auto_id,
							':md' => $model,
							':mk' => $make,
							':yr' => $year,
							':mi' => $mileage)
						);
					$success = "Record edited";
					$_SESSION['success'] = $success;
					
					header("Location: index.php");
					return;					
				}catch(Exception $ex){
					echo '<h3>There was an error, please contact support</h3>';
					error_log("edit.php, SQL error= ".$ex->getMessage());
					return;
				}
			}
			$_SESSION['error'] = $failure;
			header("Location: edit.php?auto_id=".$_REQUEST['auto_id']);
			return;
		}
		if ( isset($_POST['cancel'])  ) {		
			header("Location: index.php");
			return;
		}		
	}
?>
<!DOCTYPE html>
<html>
	<head>
	<?php require_once "bootstrap.php"; ?>
		<title>Juan Munoz's Login Page</title>
		<style>
			table, th, td {
			  border: 1px solid black;
			}
		</style>
		<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
	</head>
	<body>
	<div class="container">
		<h2>Edit record</h2>
		<?
			if( isset($_SESSION['error']) ){
				echo '<p style="color: red;">'.htmlentities($_SESSION['error'])."</p>\n";
				unset($_SESSION['error']);
			}		
		?>
		<form method="post">
			<div class="form-row">
				<div class="col">
					<br>
					<label>Make</label><br>
					<input type="text" class="form-control col-sm-1" name="make" id="make" value="<? echo $make ?>">
				</div>
			</div>
			<div class="form-row">
				<div class="col">
					<br>
					<label>Model</label><br>
					<input type="text" class="form-control col-sm-1" name="model" id="model" value="<? echo $model ?>">
				</div>
			</div>
			<div class="form-row">
				<div class="col">
					<br>
					<label>Year</label><br>
					<input type="text" class="form-control col-sm-1" name="year" id="year" value="<? echo $year ?>">
				</div>
			</div>
			<div class="form-row">
				<div class="col">
					<br>
					<label>Mileage</label><br>
					<input type="text" class="form-control col-sm-1" name="mileage" id="mileage" value="<? echo $mileage ?>">
				</div>
			</div>
			<div class="form-row">
				<div class="col">
					<br>
					<input type="submit" class="btn btn-success" name="edit" value="Save">&nbsp; &nbsp;
					<input type="submit" class="btn btn-primary" name="cancel" value="Cancel">
				</div>
			</div>			
		</form>
	</div>
	</body>
</html>
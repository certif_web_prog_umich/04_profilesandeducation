<?
	require_once('pdo.php');
	session_start();
	if( $_SESSION['name']  == '')
		die('Not logged in');
	else{
		///get the autos list
		$name = $_SESSION['name'];
		try{
			unset($stmt);
			$stmt = $link->query("SELECT * FROM autos");
			$cant_reg = 0;
			while($row = $stmt->fetch(PDO::FETCH_ASSOC)){
				$make = htmlentities($row['make']);
				$year = $row['year'];
				$mileage = $row['mileage'];
				
				$autosArr[] = $year.' '.$make.' / '.$mileage;
				$cant_reg++;
			}
		}catch(Exception $ex){
			echo '<h3>There was an error, please contact support</h3>';
			error_log("view.php, SQL error= ".$ex->getMessage());
			return;
		}	
	}
?>
<!DOCTYPE html>
<html>
<head>
<?php require_once "bootstrap.php"; ?>
<title>Juan Munoz's Login Page</title>
</head>
<body>
<div class="container">
		<div>
			<? 
				echo '<h2>Tracking Autos for '.$name.'</h2>'; 

				if( isset($_SESSION['success']) ){
					echo '<p style="color: green;">'.htmlentities($_SESSION['success'])."</p>\n";
					unset($_SESSION['success']);
				}
				
			?>
		</div>
		<div>
			<?
				
				if($cant_reg > 0){
					echo '<h3>Automobiles</h3>';
					///print_r($autosArr);
					echo '<ul>';
						foreach($autosArr as $auto){
							echo '<li>'.$auto.'</li>';
						}
					echo '</ul>';
				}
				
				
				
			?>
		</div>
		<a href="add.php">Add New </a> | <a href="logout.php"> Logout</a>
</div>
</body>
</html>		
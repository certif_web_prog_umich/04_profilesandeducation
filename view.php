<?
	require_once('pdo.php');
	require_once('util.php');
	session_start();

	///get the profile
	if( trim($_GET['profile_id']) == '' ){
		header(' location = index.php');
		return;
	}else{
		$profile_id = trim($_GET['profile_id']);
		$qry="SELECT * FROM Profile WHERE profile_id=:profile_id";

		try{
			$stmt = $link->prepare($qry);
			$stmt->execute(array(
				':profile_id' => $profile_id)
				);

			$row = $stmt->fetch(PDO::FETCH_ASSOC);

			if( $row == true ){
				$first_name = htmlentities(trim($row['first_name']));
				$last_name = htmlentities(trim($row['last_name']));
				$email = htmlentities(trim($row['email']));
				$headline = htmlentities(trim($row['headline']));
				$summary = htmlentities(trim($row['summary']));
			}

		}catch(Exception $ex){
			echo '<h3>There was an error, please contact support</h3>';
			error_log("view.php, SQL error= ".$ex->getMessage());
			return;
		}
	}
	///Load the position rows
	$positions = loadPos($link, $_REQUEST['profile_id']);
	$educations = loadEdu($link, $_REQUEST['profile_id']);
?>
<!DOCTYPE html>
<html>
<head>
<?php require_once "bootstrap.php"; ?>
	<title>Juan Munoz's Login Page</title>
	<style>
		table, th, td {
		  border: 1px solid black;
		}
	</style>
	<!--
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
	-->
	<? require_once('head.php'); ?>
</head>
<body>
<div class="container">
		<div>
			<?
				if( isset($_SESSION['error']) ){
					echo '<p style="color: red;">'.htmlentities($_SESSION['error'])."</p>\n";
					unset($_SESSION['error']);
				}
				if( isset($_SESSION['success']) ){
					echo '<p style="color: green;">'.htmlentities($_SESSION['success'])."</p>\n";
					unset($_SESSION['success']);
				}

			?>
		</div>
		<h1>Profile Information</h1>
		<br>
		<table class="table">
			<thead class="thead-dark">
				<tr>
					<th>First Name</th>
					<th>Last Name</th>
					<th>Email</th>
					<th>Headline</th>
					<th>Summary</th>
				</tr>
			<thead >
				<tr>
					<td ><? echo $first_name ?></td>
					<td ><? echo $last_name ?></td>
					<td ><? echo $email ?></td>
					<td ><? echo $headline ?></td>
					<td ><? echo $summary ?></td>
				</tr>
		</table>
		<br>
		<p>Education</p>
		<ul>
			<?
				foreach ($educations as $education) {
					$year = htmlentities($education['year']);
					$name = htmlentities($education['name']);
					echo '<li>'.$year.' : '.$name.'</li>';
				}
			?>
		</ul>
		<br>
		<p>Position</p>
		<ul>
			<?
				foreach ($positions as $position) {
					$year = htmlentities($position['year']);
					$description = htmlentities($position['description']);
					echo '<li>'.$year.' : '.$description.'</li>';
				}
			?>
		</ul>
		<br>
		<a href="index.php">Done</a>

		</div>

</div>
</body>
</html>

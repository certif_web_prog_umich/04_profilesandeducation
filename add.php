<?
	require_once('pdo.php');
	require_once('util.php');
	session_start();
	if( $_SESSION['name'] == '')
		die("ACCESS DENIED");
	else{
		$name = $_SESSION['name'];
		$user_id = $_SESSION['user_id'];
		$_arroba = "@";

		if ( isset($_POST['cancel'])  ) {
			header("Location: index.php");
			return;
		}

		if ( isset($_POST['add'])  ) {
			$validate  = validatePos();

			///if( ($validate == "All fields are required") || ($validate == "Position year must be numeric") ){
			if( $validate == 1 ){

					$first_name = trim($_POST['first_name']);
					$last_name = trim($_POST['last_name']);
					$email = trim($_POST['email']);
					$headline = trim($_POST['headline']);
					$summary = trim($_POST['summary']);

					$_SESSION['first_name'] = $first_name;
					$_SESSION['last_name'] = $last_name;
					$_SESSION['email'] = $email;
					$_SESSION['headline'] = $headline;
					$_SESSION['summary'] = $summary;

					///To do: refactor this to util.php
					$okey = 1;
					if( ($first_name == '') || ($last_name == '') || ($email == '') || ($headline == '') || ($summary == '') ){
						$okey = 0;
						$failure = "All fields are required";
					///}elseif((!is_numeric($year)) || (!is_numeric($mileage))){
					}elseif (strpos($email, $_arroba) === false){
						$okey = 0;
						$failure = $failure." Email must have an at-sign (@)";
					}
					///
					
					///Validate year fields
					$okey = validYear("E");
					if($okey == 0 )
						$failure = "Year must be numeric";
					
					if( $okey == 1 ){
						$qry = "INSERT INTO Profile
										(user_id, first_name, last_name, email, headline, summary)
										VALUES (:user_id, :first_name, :last_name, :email, :headline, :summary)
										";
						try{
							$stmt = $link->prepare($qry);
							$stmt->execute(array(
									':user_id' => $user_id,
									':first_name' => $first_name,
									':last_name' => $last_name,
									':email' => $email,
									':headline' => $headline,
									':summary' => $summary)
								);

								$profile_id = $link->lastInsertId();

								// Insert the position entries
								insertPositions($link, $profile_id);
								usleep($waitFor);
								insertEducations($link, $profile_id);
								/*
								$rank = 1;
								for($i=1; $i<=9; $i++) {
										if ( ! isset($_POST['year'.$i]) ) continue;
										if ( ! isset($_POST['desc'.$i]) ) continue;
										$year = $_POST['year'.$i];
										$desc = $_POST['desc'.$i];

										$stmt = $link->prepare('INSERT INTO Position
												(profile_id, rank, year, description)
										VALUES ( :pid, :rank, :year, :desc)');
										$stmt->execute(array(
												':pid' => $profile_id,
												':rank' => $rank,
												':year' => $year,
												':desc' => $desc)
										);
										$rank++;
								}
								*/



							$success = "Profile added";
							$_SESSION['success'] = $success;

							header("Location: index.php");
							return;
						}catch(Exception $ex){
							echo '<h3>There was an error, please contact support</h3>';
							error_log("add.php, SQL error= ".$ex->getMessage());
							return;
						}
					}
					$_SESSION['error'] = $failure;
					header("Location: add.php");
					return;

				}else{
				///$failure = $validate;

				$_SESSION['error'] = $validate;
				header("Location: add.php");
				return;
			}

		}
	}
?>
<!DOCTYPE html>
<html>
<head>
<?php require_once "bootstrap.php"; ?>
<title>Juan Munoz's Login Page</title>
<!--
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
-->
<? require_once('head.php'); ?>
</head>
<body>
<div class="container">
	<h2>Adding Profile for <? echo $name ?></h2>
	<?
		flashMessages();

		if( isset($_SESSION['first_name']) ){
			$first_name = $_SESSION['first_name'] ;
			unset($_SESSION['first_name']);
		}

		if( isset($_SESSION['last_name']) ){
			$last_name = $_SESSION['last_name'] ;
			unset($_SESSION['last_name']);
		}
		if( isset($_SESSION['email']) ){
			$email = $_SESSION['email'] ;
			unset($_SESSION['email']);
		}
		if( isset($_SESSION['headline']) ){
			$headline = $_SESSION['headline'] ;
			unset($_SESSION['headline']);
		}
		if( isset($_SESSION['summary']) ){
			$summary = $_SESSION['summary'] ;
			unset($_SESSION['summary']);
		}
	?>
		<div>
		<form method="post">
			<div class="form-row">
				<div class="col">
					<br>
					<label>First Name</label><br>
					<input type="text" class="form-control col-sm-4" name="first_name" id="first_name" value="<? echo $first_name ?>">
				</div>
			</div>
			<div class="form-row">
				<div class="col">
					<br>
					<label>Last Name</label><br>
					<input type="text" class="form-control col-sm-4" name="last_name" id="last_name" value="<? echo $last_name ?>">
				</div>
			</div>
			<div class="form-row">
				<div class="col">
					<br>
					<label>Email</label><br>
					<input type="text" class="form-control col-sm-4" name="email" id="email" value="<? echo $email ?>">
				</div>
			</div>
			<div class="form-row">
				<div class="col">
					<br>
					<label>Headline</label><br>
					<input type="text" class="form-control col-sm-4" name="headline" id="headline" value="<? echo $headline ?>">
				</div>
			</div>
			<div class="form-row">
				<div class="col">
					<br>
					<label>Summary</label><br>
					<input type="text" class="form-control col-sm-4" name="summary" id="summary" value="<? echo $summary ?>">
				</div>
			</div>
			<br>
			<div class="form-row">
			<div>
			<br>

			<br>
			<?
				$countEdu = 0;

				echo ('<p>Education: <input type ="submit" id="addEdu" value="+">'."\n");
				echo ('<div id="edu_fields">'."\n");
				if( count($schools) > 0 ){
					foreach( $schools as $school ){
						$countEdu++;
						echo('<div id="edu'.$countEdu.'">');
						echo
						'<p>Year: <input type="text" name="edu_year'.$countEdu.'" value="'.$school['year'].'" />
						<input type="button" value="-" onclick="$(\'#edu'.$countEdu.'\').remove();return false;"></p>
						<p>School: <input type="text" size="80" name="edu_school'.$countEdu.'" class="school"
						value="'.htmlentities($school['name']).'"/>';
						echo "\n</div>\n";
					}
				}
				echo("</div></p>\n");

			?>
			<div>
			<br>

			<div class="form-row">
				<div class="col">
					Position: <input type="submit" id="addPos" value="+">
					<div id="position_fields">
					</div>
				</div>
			</div>

			<div class="form-row">
				<div class="col">
					<br>
					<p>
						<input type="submit" class="btn btn-success" name="add" value="Add">&nbsp; &nbsp;
						<input type="submit" class="btn btn-primary" name="cancel" value="Cancel">
				</p>
				</div>
			</div>
		</form>

		<script>
		countPos = 0;
		countEdu = 0;
		// http://stackoverflow.com/questions/17650776/add-remove-html-inside-div-using-javascript
		$(document).ready(function(){
		    window.console && console.log('Document ready called');
		    $('#addPos').click(function(event){
		        // http://api.jquery.com/event.preventdefault/
		        event.preventDefault();
		        if ( countPos >= 9 ) {
		            alert("Maximum of nine position entries exceeded");
		            return;
		        }
		        countPos++;
		        window.console && console.log("Adding position "+countPos);

		        $('#position_fields').append(
		            '<div id="position'+countPos+'"> \
		            <p>Year: <input type="text" name="year'+countPos+'" value="" /> \
		            <input type="button" value="-" \
		                onclick="$(\'#position'+countPos+'\').remove();return false;"></p> \
		            <textarea name="desc'+countPos+'" rows="8" cols="80"></textarea>\
		            </div>');
		    });

		    $('#addEdu').click(function(event){
		        // http://api.jquery.com/event.preventdefault/
		        event.preventDefault();
		        if ( countEdu >= 9 ) {
		            alert("Maximum of nine education entries exceeded");
		            return;
		        }
		        countEdu++;
		        window.console && console.log("Adding education "+countEdu);

						// Grab some HTML with hot spots and insert into the DOM
						var source = $("#edu-template").html();
						$('#edu_fields').append(source.replace(/@COUNT@/g,countEdu));

						// Add the event handler to the new ones
						$('.school').autocomplete({
							source: "school.php"
						}).attr('autocomplete', 'on');

		    });

				$('.school').autocomplete({
					source: "school.php"
				}).attr('autocomplete', 'on');
		});
		</script>
        <!--  HTML with Substitution hot spots -->
        <script id="edu-template" type="text">
            <div id="edu@COUNT@">
                    <p>Year: <input type ="text" name="edu_year@COUNT@" value="" />
                    <input type="button" value="-" onclick="$('#edu@COUNT@').remove();return false;"><br>
                    <p>School: <input type="text" size="80" name="edu_school@COUNT@" class="school" value="" />
                    </p>
            </div>

        </script>

		</div>
		<!-- <a href="add.php">Add New </a> | <a href="logout.php"> Logout</a> -->
</div>
</body>
</html>

<?

    require_once('util.php');
    ////session_start();

    if ( !isset($_GET['term']) ) die('Missing required parameter');

    ///Lets not start a session unless we already have one
    if ( ! isset($_COOKIE[session_name()]) ){
      die("Must be logged in");
    }

    session_start();

    if ( ! isset($_SESSION['user_id']) ){
      die('ACCESS DENIED');
    }
    /// Don't even make a database connection until we're happy
    require_once('pdo.php');

    header('Content-type: application/json; charset=utf-8');

    $term = $_GET['term'];
    error_log('Looking up typhead term ='.$term);

    $stmt = $link->prepare("SELECT name FROM Institution WHERE name LIKE :prefix");
    $stmt->execute(array( ':prefix' => $term."%"));

    $retval = array();
    while ( $row = $stmt->fetch(PDO::FETCH_ASSOC) ) {
        $retval[] = $row['name'];
    }

    echo(json_encode($retval, JSON_PRETTY_PRINT));
    ///$stmt->execute(array( ':prefix' => $_REQUEST['term']."%"));
    ///try{
      ///$stmt->execute(array( ':prefix' => $term."%"));

      /*
    }catch(Exception $ex){
      echo '<h3>There was an error, please contact support</h3>';
      error_log("school.php, SQL error= ".$ex->getMessage());
      return;
    }
    */


    /*
    if( $_SESSION['name'] == '')

    else{
        $stmt = $pdo->prepare('SELECT name FROM Institution WHERE name LIKE :prefix');
        $stmt->execute(array( ':prefix' => $_REQUEST['term']."%"));

        $retval = array();
        while ( $row = $stmt->fetch(PDO::FETCH_ASSOC) ) {
            $retval[] = $row['name'];
        }

        echo(json_encode($retval, JSON_PRETTY_PRINT));
    }
    */

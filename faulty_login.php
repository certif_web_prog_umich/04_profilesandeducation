<?php // Do not put any HTML above this line

	require_once('pdo.php');
	

	if ( isset($_POST['cancel'] ) ) {
		// Redirect the browser to game.php
		header("Location: index.php");
		return;
	}

	$salt = 'XyZzy12*_';
	$stored_hash = '1a52e17fa899cf40fb04cfc42e6352f1';///'a8609e8d62c043243c4e201cbb342862';  // Pw is meow123

	$failure = false;  // If we have no POST data

	// Check to see if we have some POST data, if we do process it
	$who = $_POST['who'];
	$pass = $_POST['pass'];
	$_arroba = "@";

	session_start();
	
	if ( isset($who) && isset($pass) ) {
		if ( strlen($who) < 1 || strlen($pass) < 1 ) {
			$failure = "Email name and password are required";
		} else {
			///email has an @
			if (strpos($who, $_arroba) === false){
				$failure = "Email must have an at-sign (@)";
			}
			else{
				$check = hash('md5', $salt.$_POST['pass']);
				if ( $check == $stored_hash ) {
					// Redirect the browser to game.php
					///header("Location: game.php?name=".urlencode($_POST['who']));
					error_log("Login success ".$_POST['who']);
					///header("Location: view.php?name=".urlencode($who));
					$_SESSION['name'] = $_POST['email'];
					///header("Location: view.php");
					///return;
				} else {
					$failure = "Incorrect password";
					error_log("Login fail ".$_POST['who']." $check");
				}
			}
		}
		//$_SESSION['error'] = $failure;
		//header("Location: login.php");
		//return;		
	}	

	// Fall through into the View
?>
<!DOCTYPE html>
<html>
<head>
<?php require_once "bootstrap.php"; ?>
<title>Juan Munoz's Login Page</title>
</head>
<body>
<div class="container">
<h1>Please Log In</h1>
<?php
	// Note triple not equals and think how badly double
	// not equals would work here...
	if ( isset($_SESSION['error']) ) {
		echo('<p style="color: red;">'.htmlentities($_SESSION['error'])."</p>\n");
		unset($_SESSION['error']);
	}	
	/*
	if ( $failure !== false ) {
		// Look closely at the use of single and double quotes
		echo('<p style="color: red;">'.htmlentities($failure)."</p>\n");
	*/
}
?>
<form method="POST">
	<label for="nam">email</label>
	<input type="text" name="who" id="nam"><br/>
	<label for="id_1723">Password</label>
	<input type="password" name="pass" id="id_1723"><br/>
	<input type="submit" value="Log In">
	<input type="submit" name="cancel" value="Cancel">
</form>
<p>
For a password hint, view source and find a password hint
in the HTML comments.
<!-- Hint: The password is the three character of the coding language used (all lower case) followed by 123. -->
</p>
</div>
</body>

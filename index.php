<?
	require_once('pdo.php');
	require_once('util.php');
	session_start();
	$loggedin = 0;
	if( $_SESSION['name']  == ''){
		$loggedin = 0;
	}
	else{
		$loggedin = 1;
	}

	try{
		unset($stmt);
		$stmt = $link->query("SELECT * FROM Profile");
		$cant_reg = 0;
		while($row = $stmt->fetch(PDO::FETCH_ASSOC)){
			$profile_id = $row['profile_id'];
			$user_id = $row['user_id'];
			$first_name = htmlentities(trim($row['first_name']));
			$last_name = htmlentities(trim($row['last_name']));
			$email = htmlentities(trim($row['email']));
			$headline = htmlentities(trim($row['headline']));
			$summary = htmlentities(trim($row['summary']));

			$arrProfileId[] = $profile_id;
			$arrUserId[] = $user_id;
			$arrFirstName[] = $first_name;
			$arrLastName[] = $last_name;
			$arrEmail[] = $email;
			$arrEmail[] = $email;
			$arrHeadline[] = $headline;
			$arrSummary[] = $summary;

			$cant_reg++;
		}
	}catch(Exception $ex){
		echo '<h3>There was an error, please contact support</h3>';
		error_log("view.php, SQL error= ".$ex->getMessage());
		return;
	}

?>
<!DOCTYPE html>
<html>
<head>
<?php require_once "bootstrap.php"; ?>
	<title>Juan Munoz's Login Page</title>
	<style>
		table, th, td {
		  border: 1px solid black;
		}
	</style>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>
<body>
<div class="container">
		<div>
			<?
				flashMessages();
			?>
		</div>
		<h1>Juan Munoz Resume Registry</h1>
		<br>
		<?
			if( $loggedin == 0)
				echo '<a href="login.php">Please log in</a>';
			else
				echo '<a href="logout.php">Logout</a>';
		?>
			<h1>Profiles</h1>
			<?
				if($cant_reg > 0){
					///Reset
					$cant_reg = 0;

					echo '<table class="table">';
						echo '<thead class="thead-dark">';
							echo '<tr>';
								echo '<th hidden>id</th>';
								echo '<th>Name</th>';
								echo '<th>Headline</th>';
								if( $loggedin == 1)
									echo '<th>Action</th>';
							echo '</tr>';
						echo '<thead >';
						foreach($arrFirstName as $name){
							$profile_id = $arrProfileId[$cant_reg];
							$headline = $arrHeadline[$cant_reg];
							echo '<tr>';
								echo '<td hidden>'.($profile_id).'</td>';
								echo '<td><a href="view.php?profile_id='.urlencode($profile_id).'" >'.$name.'</a></td>';
								echo '<td>'.$headline.'</td>';
								if( $loggedin == 1)
									echo '<td><a href="edit.php?profile_id='.urlencode($profile_id).'" >Edit</a> | <a href="delete.php?profile_id='.urlencode($profile_id).'" >Delete</a></td>';
							echo '</tr>';
							$cant_reg++;
						}
					echo '</table>';
				}
				else
					echo '<h2>No rows found !</h2>';
			?>
			<br>
			<?
				if( $loggedin == 1)
					echo '<a href="add.php">Add New Entry</a>';
			?>
		</div>

</div>
</body>
</html>

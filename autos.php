<?php // Do not put any HTML above this line

	require_once('pdo.php');

	// Demand a GET parameter
	if ( ! isset($_GET['name']) || strlen($_GET['name']) < 1  ) {
		die('Name parameter missing');
	}

	$name = $_GET['name'];

	if ( isset($_POST['cancel'] ) ) {
		// Redirect the browser to game.php
		header("Location: index.php");
		return;
	}

	$salt = 'XyZzy12*_';
	$stored_hash = '1a52e17fa899cf40fb04cfc42e6352f1';///'a8609e8d62c043243c4e201cbb342862';  // Pw is meow123

	$failure = false;  // If we have no POST data
	$success = false;  // If we inserted data

	// Check to see if we have some POST data, if we do process it
	if ( isset($_POST['add'])  ) {
		$make = trim($_POST['make']);
		$year = trim($_POST['year']);
		$mileage = trim($_POST['mileage']);
		
		if((!is_numeric($year)) || (!is_numeric($mileage))){
			$failure = "Mileage and year must be numeric";
		}else if($make == ""){
			$failure = "Make is required";
		}else{
			$qryInsert = "INSERT INTO autos
							(make, year, mileage)
							VALUES (:mk, :yr, :mi)
							";
			try{
				$stmt = $link->prepare($qryInsert);
				$stmt->execute(array(
						':mk' => $make,
						':yr' => $year,
						':mi' => $mileage)
					);
				$success = "Record inserted";
			}catch(Exception $ex){
				echo '<h3>There was an error, please contact support</h3>';
				error_log("autos.php, SQL error= ".$ex->getMessage());
				return;
			}
		}
		
		
	}else
	if ( isset($_POST['logout']) ) {
		header('Location: index.php');
	}

	///get the autos list
	try{
		unset($stmt);
		$stmt = $link->query("SELECT * FROM autos");
		$cant_reg = 0;
		while($row = $stmt->fetch(PDO::FETCH_ASSOC)){
			$make = htmlentities($row['make']);
			$year = $row['year'];
			$mileage = $row['mileage'];
			
			$autosArr[] = $year.' '.$make.' / '.$mileage;
			$cant_reg++;
		}
	}catch(Exception $ex){
		echo '<h3>There was an error, please contact support</h3>';
		error_log("autos.php, SQL error= ".$ex->getMessage());
		return;
	}

// Fall through into the View
?>
<!DOCTYPE html>
<html>
<head>
<?php require_once "bootstrap.php"; ?>
<title>Juan Munoz's Autos Page</title>
</head>
<body>
	<div class="container">
		<?php
			// Note triple not equals and think how badly double
			// not equals would work here...
			if ( $failure !== false ) {
				// Look closely at the use of single and double quotes
				echo('<p style="color: red;">'.htmlentities($failure)."</p>\n");
			}
			if ( $success !== false ) {
				// Look closely at the use of single and double quotes
				echo('<p style="color: green;">'.htmlentities($success)."</p>\n");
			}	
			echo '<h2>Tracking Autos for</h2>';
			echo '<h2>'.$name.'</h2>';
		?>

		<form method="POST">
			<label for="make">Make:</label>
			<input type="text" name="make" id="make"><br/>
			<label for="year">Year:</label>
			<input type="text" name="year" id="year"><br/>
			<label for="mileage">Mileage:</label>
			<input type="text" name="mileage" id="mileage"><br/>	
			<input type="submit" name="add" value="Add">
			<input type="submit" name="logout" value="Logout">
		</form>
		<div>
			<?
				
				if($cant_reg > 0){
					echo '<h3>Automobiles</h3>';
					///print_r($autosArr);
					echo '<ul>';
						foreach($autosArr as $auto){
							echo '<li>'.$auto.'</li>';
						}
					echo '</ul>';
				}
				
				
				
			?>
		</div>
	</div>
</body>

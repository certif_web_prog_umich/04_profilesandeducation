<?
	require_once('pdo.php');
	require_once('util.php');
	session_start();
	if( $_SESSION['name'] == '')
		die("ACCESS DENIED");
	else{
		if( trim($_GET['profile_id']) == '' ){
			header("Location: index.php");
			return;
		}
		$profile_id = trim($_GET['profile_id']);
		$name = $_SESSION['name'];
		$user_id = $_SESSION['user_id'];
		$_arroba = "@";

		$stmt = $link->prepare('SELECT * FROM Profile
			WHERE profile_id = :profile_id');
		$stmt->execute(array( ':profile_id' => $profile_id));
		$row = $stmt->fetch(PDO::FETCH_ASSOC);
		if ( $row == true ) {
			$first_name = htmlentities(trim($row['first_name']));
			$last_name = htmlentities(trim($row['last_name']));
			$email = htmlentities(trim($row['email']));
			$headline = htmlentities(trim($row['headline']));
			$summary = htmlentities(trim($row['summary']));

		}

		if ( isset($_POST['cancel'])  ) {
			header("Location: index.php");
			return;
		}

		if ( isset($_POST['edit'])  ) {
			$validate  = validatePos();

			if( $validate == 1 ){
					$profile_id = trim($_POST['profile_id']);
					$first_name = trim($_POST['first_name']);
					$last_name = trim($_POST['last_name']);
					$email = trim($_POST['email']);
					$headline = trim($_POST['headline']);
					$summary = trim($_POST['summary']);

					$okey = 1;
					if( ($first_name == '') || ($last_name == '') || ($email == '') || ($headline == '') || ($summary == '') ){
						$okey = 0;
						$failure = "All fields are required";
					}if (strpos($email, $_arroba) === false){
						$okey = 0;
						$failure = $failure." Email address must contain @";
					}
					if( $okey == 1 ){
					    //Update profile
                        $qry = "UPDATE Profile SET 
					            first_name=:first_name,
					             last_name=:last_name,
                                email=:email,
                                headline=:headline,
                                summary=:summary
                                 WHERE profile_id=:profile_id";

                        try{
                            $stmt = $link->prepare($qry);
                            $stmt->execute(array(
                                    ':profile_id' => $profile_id,
                                    ':first_name' => $first_name,
                                    ':last_name' => $last_name,
                                    ':email' => $email,
                                    ':headline' => $headline,
                                    ':summary' => $summary)
                            );
                        }catch(Exception $ex){
                            echo '<h3>There was an error, please contact support</h3>';
                            error_log("edit.php, SQL error= ".$ex->getMessage());
                            return;
                        }
					    
						// Clear out the old position entries
					    $stmt = $link->prepare('DELETE FROM Position
					        WHERE profile_id=:pid');
					    $stmt->execute(array( ':pid' => $_REQUEST['profile_id']));

						// Insert the position entries
						insertPositions($link, $profile_id);

						
						usleep($waitFor);
						// Clear out the old education entries
					    $stmt = $link->prepare('DELETE FROM Education
					        WHERE profile_id=:pid');
					    $stmt->execute(array( ':pid' => $_REQUEST['profile_id']));
						
						// Insert the position entries
						insertEducations($link, $profile_id);
						


						/*
						try{
									// Insert the position entries
									$rank = 1;
									$arrDesc = array();

									for($i=1; $i<=9; $i++) {
											if ( ! isset($_POST['year'.$i]) ) continue;
											if ( ! isset($_POST['desc'.$i]) ) continue;
											$year = trim($_POST['year'.$i]);
											$desc = trim($_POST['desc'.$i]);

											$stmt = $link->prepare('INSERT INTO Position
													(profile_id, rank, year, description)
											VALUES ( :pid, :rank, :year, :desc)');
											$stmt->execute(array(
													':pid' => $_REQUEST['profile_id'],
													':rank' => $rank,
													':year' => $year,
													':desc' => $desc)
											);
											$rank++;
									}
						*/
							$success = "Profile edited";
							$_SESSION['success'] = $success;
						
							header("Location: index.php");
							return;

						/*
						}catch(Exception $ex){
							echo '<h3>There was an error, please contact support</h3>';
							error_log("edit.php, SQL error= ".$ex->getMessage());
							return;
						}
						*/
					}else{
						$_SESSION['error'] = $failure;
						///header("Location: add.php");
						header("Location: edit.php?profile_id=" . $_POST["profile_id"]);
						return;
					}
				}else{
				///$failure = $validate;

				$_SESSION['error'] = $validate;
				header("Location: edit.php?profile_id=" . $_POST["profile_id"]);
				return;
			}
		}
	}
	///Load the position rows
	$positions = loadPos($link, $_REQUEST['profile_id']);
    $schools = loadEdu($link, $_REQUEST['profile_id']);
?>
<!DOCTYPE html>
<html>
<head>
<?php require_once "bootstrap.php"; ?>
<title>Juan Munoz's Login Page</title>
<!--
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
-->
<? require_once('head.php'); ?>
</head>
<body>
<div class="container">
	<h2>Eding Profile for <? echo $name ?></h2>
	<?
		flashMessages();

		//echo '<br>'.$_REQUEST['profile_id'].'<br>';
		///print_r($positions);
		//echo '<br>';
		echo '<input type="text" id="cantPoss" name="cantPoss" value="'.count($positions).'" hidden>';

		if( isset($_SESSION['first_name']) ){
			$first_name = $_SESSION['first_name'] ;
			unset($_SESSION['first_name']);
		}

		if( isset($_SESSION['last_name']) ){
			$last_name = $_SESSION['last_name'] ;
			unset($_SESSION['last_name']);
		}
		if( isset($_SESSION['email']) ){
			$email = $_SESSION['email'] ;
			unset($_SESSION['email']);
		}
		if( isset($_SESSION['headline']) ){
			$headline = $_SESSION['headline'] ;
			unset($_SESSION['headline']);
		}
		if( isset($_SESSION['summary']) ){
			$summary = $_SESSION['summary'] ;
			unset($_SESSION['summary']);
		}

	?>
		<div>
		<form method="post">
			<input type="text" class="form-control col-sm-4" name="profile_id" id="profile_id" value="<? echo $profile_id ?>" hidden>
			<div class="form-row">
				<div class="col">
					<br>
					<label>First Name</label><br>
					<input type="text" class="form-control col-sm-4" name="first_name" id="first_name" value="<? echo $first_name ?>">
				</div>
			</div>
			<div class="form-row">
				<div class="col">
					<br>
					<label>Last Name</label><br>
					<input type="text" class="form-control col-sm-4" name="last_name" id="last_name" value="<? echo $last_name ?>">
				</div>
			</div>
			<div class="form-row">
				<div class="col">
					<br>
					<label>Email</label><br>
					<input type="text" class="form-control col-sm-4" name="email" id="email" value="<? echo $email ?>">
				</div>
			</div>
			<div class="form-row">
				<div class="col">
					<br>
					<label>Headline</label><br>
					<input type="text" class="form-control col-sm-4" name="headline" id="headline" value="<? echo $headline ?>">
				</div>
			</div>
			<div class="form-row">
				<div class="col">
					<br>
					<label>Summary</label><br>
					<input type="text" class="form-control col-sm-4" name="summary" id="summary" value="<? echo $summary ?>">
				</div>
			</div>
			<br>

			<?
				$countEdu = 0;

				echo ('<p>Education: <input type ="submit" id="addEdu" value="+">'."\n");
				echo ('<div id="edu_fields">'."\n");
				if( count($schools) > 0 ){
					foreach( $schools as $school ){
						$countEdu++;
						echo('<div id="edu'.$countEdu.'">');
						echo
						'<p>Year: <input type="text" name="edu_year'.$countEdu.'" value="'.$school['year'].'" />
						<input type="button" value="-" onclick="$(\'#edu'.$countEdu.'\').remove();return false;"></p>
						<p>School: <input type="text" size="80" name="edu_school'.$countEdu.'" class="school"
						value="'.htmlentities($school['name']).'"/>';
						echo "\n</div>\n";
					}
				}
				echo("</div></p>\n");

			?>

<br>
<p>Position: <input type="submit" id="addPos" value="+">
<div id="position_fields">
<?
$countPos = 1;
foreach ($positions as $position) {
$year = htmlentities($position['year']);
$description = htmlentities($position['description']);
?>
<div id="<? echo 'position'.$countPos ?>">
<p>Year: <input type="text" name="<? echo 'year'.$countPos ?>" value="<? echo $year ?>" />
<input type="button" value="-" onclick="$('#position<? echo $countPos ?>').remove();return false;">
</p>
<textarea name="<? echo 'desc'.$countPos ?>" rows="8" cols="80">
	<? echo $description ?>
</textarea>
</div>
<?
$countPos++;
}
?>
</div>

			</p>

			<div class="form-row">

				<div class="col">
					<br>
					<input type="submit" class="btn btn-success" name="edit" value="Save">&nbsp; &nbsp;
					<input type="submit" class="btn btn-primary" name="cancel" value="Cancel">
				</div>
			</div>
		</form>

		<script src="js/jquery-1.10.2.js"></script>
		<script src="js/jquery-ui-1.11.4.js"></script>
		<script>
		countPos = <? echo count($positions) ?>;
		countEdu = <? echo count($schools) ?>;

		// http://stackoverflow.com/questions/17650776/add-remove-html-inside-div-using-javascript
		$(document).ready(function(){
		    window.console && console.log('Document ready called');
		    $('#addPos').click(function(event){
		        // http://api.jquery.com/event.preventdefault/
		        event.preventDefault();
		        if ( countPos >= 9 ) {
		            alert("Maximum of nine position entries exceeded");
		            return;
		        }
		        countPos++;
		        window.console && console.log("Adding position "+countPos);

		        $('#position_fields').append(
		            '<div id="position'+countPos+'"> \
		            <p>Year: <input type="text" name="year'+countPos+'" value="" /> \
		            <input type="button" value="-" \
		                onclick="$(\'#position'+countPos+'\').remove();return false;"></p> \
		            <textarea name="desc'+countPos+'" rows="8" cols="80"></textarea>\
		            </div>');
		    });

		    $('#addEdu').click(function(event){
		        // http://api.jquery.com/event.preventdefault/
		        event.preventDefault();
		        if ( countEdu >= 9 ) {
		            alert("Maximum of nine education entries exceeded");
		            return;
		        }
		        countEdu++;
		        window.console && console.log("Adding education "+countEdu);

						// Grab some HTML with hot spots and insert into the DOM
						var source = $("#edu-template").html();
						$('#edu_fields').append(source.replace(/@COUNT@/g,countEdu));

						// Add the event handler to the new ones
						$('.school').autocomplete({
							source: "school.php"
						}).attr('autocomplete', 'on');

		    });

				$('.school').autocomplete({
					source: "school.php"
				}).attr('autocomplete', 'on');
		});
		</script>
        <!--  HTML with Substitution hot spots -->
        <script id="edu-template" type="text">
            <div id="edu@COUNT@">
                    <p>Year: <input type ="text" name="edu_year@COUNT@" value="" />
                    <input type="button" value="-" onclick="$('#edu@COUNT@').remove();return false;"><br>
                    <p>School: <input type="text" size="80" name="edu_school@COUNT@" class="school" value="" />
                    </p>
            </div>

        </script>

		</div>
</div>
</body>
</html>

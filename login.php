<?php // Do not put any HTML above this line

require_once('pdo.php');
session_start();

if ( isset($_POST['cancel'] ) ) {
    // Redirect the browser to game.php
    header("Location: index.php");
    return;
}

$salt = 'XyZzy12*_';

$failure = false;  // If we have no POST data

// Check to see if we have some POST data, if we do process it
$who = $_POST['email'];
$pass = $_POST['pass'];
$_arroba = "@";

if ( isset($who) && isset($pass) ) {
    if ( strlen($who) < 1 || strlen($pass) < 1 ) {
        $failure = "User name and password are required";
    } else {
		///email has an @
		if (strpos($who, $_arroba) === false){
			$failure = "Email must have an at-sign (@)";
		}
		else{
			$check = hash('md5', $salt.$_POST['pass']);
			$stmt = $link->prepare('SELECT user_id, name FROM users
				WHERE email = :em AND password = :pw');
			$stmt->execute(array( ':em' => $_POST['email'], ':pw' => $check));
			$row = $stmt->fetch(PDO::FETCH_ASSOC);
			if ( $row == true ) {
				$_SESSION['name'] = $row['name'];
				$_SESSION['user_id'] = $row['user_id'];
				// Redirect the browser to index.php
				error_log("Login success ".$_POST['name']);
				header("Location: index.php");
				return;
			} else {
				$failure = "Incorrect password";
				error_log("Login fail ".$_POST['who']." $check");
			}
		}
    }
	$_SESSION['error'] = $failure;
	header("Location: login.php");
	return;
}

// Fall through into the View
?>
<!DOCTYPE html>
<html>
<head>
<?php require_once "bootstrap.php"; ?>
<title>Juan Munoz's Login Page</title>

<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>
<body>
<div class="container">
<h1>Please Log In</h1>
<?php
	// Note triple not equals and think how badly double
	// not equals would work here...

	if( isset($_SESSION['error']) ){
		echo '<p style="color: red;">'.htmlentities($_SESSION['error'])."</p>\n";
		unset($_SESSION['error']);
	}

?>
<form method="POST">
	<label for="nam">User Name</label>
	<input type="text" name="email" id="email" value="umsi@umich.edu"><br/>
	<label for="id_1723">Password</label>
	<input type="password" name="pass" id="id_1723"><br/>
	<input type="submit" onclick="return doValidate();" value="Log In" >
	<input type="submit" name="cancel" value="Cancel">
</form>
<p>
For a password hint, view source and find a password hint
in the HTML comments.
<!-- Hint: The password is the three character of the coding language used (all lower case) followed by 123. -->
</p>
</div>
</body>
<script>
	function doValidate() {
		console.log('Validating...');
		try {
			email = document.getElementById('email').value;
			pw = document.getElementById('id_1723').value;

			console.log("Validating email and pwd");
			if ( (email == null || email == "") && (pw == null || pw == "")) {
				alert("Please enter an email and password");
				return false;
			}

			console.log("Validating pw="+pw);
			if (pw == null || pw == "") {
				alert("Password is missing");
				return false;
			}

			console.log("Validating email="+email);
			if (email == null || email == "") {
				alert("Email is missing");
				return false;
			}else{
				if( !validateEmail(email) ){
					alert("Email must have an at-sign (@)");
					return false;
				}
				return true;
			}



			return true;
		} catch(e) {
			return false;
		}
		return false;
	}

	function validateEmail(email) {
	  const re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
	  return re.test(email);
	}
</script
</html>

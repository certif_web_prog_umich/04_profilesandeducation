<?
	require_once('pdo.php');
	session_start();
	if( $_SESSION['name'] == '')
		die("ACCESS DENIED");
	else{
		if( trim($_GET['profile_id']) == '' ){
			header("Location: index.php");
			return;			
		}
		
		$profile_id = trim($_GET['profile_id']);
		
		$name = $_SESSION['name'];
		$user_id = $_SESSION['user_id'];		
		$_arroba = "@";
		
		/*
		unset($_SESSION['model']);
		unset($_SESSION['make']);
		unset($_SESSION['year']);
		unset($_SESSION['mileage']);
		*/
		
		$stmt = $link->prepare('SELECT * FROM Profile
			WHERE profile_id = :profile_id');
		$stmt->execute(array( ':profile_id' => $profile_id));
		$row = $stmt->fetch(PDO::FETCH_ASSOC);
		if ( $row == true ) {		
			$first_name = htmlentities(trim($row['first_name']));
			$last_name = htmlentities(trim($row['last_name']));
			$email = htmlentities(trim($row['email']));
			$headline = htmlentities(trim($row['headline']));
			$summary = htmlentities(trim($row['summary']));		
		}

		if ( isset($_POST['edit'])  ) {
			$first_name = trim($_POST['first_name']);
			$last_name = trim($_POST['last_name']);
			$email = trim($_POST['email']);
			$headline = trim($_POST['headline']);
			$summary = trim($_POST['summary']);
			
			$_SESSION['first_name'] = $first_name;
			$_SESSION['last_name'] = $last_name;
			$_SESSION['email'] = $email;
			$_SESSION['headline'] = $headline;
			$_SESSION['summary'] = $summary;
					
			$okey = 1;
			if( ($first_name == '') || ($last_name == '') || ($email == '') || ($headline == '') || ($summary == '') ){
				$okey = 0;
				$failure = "All fields are required";
			///}elseif((!is_numeric($year)) || (!is_numeric($mileage))){
			}if (strpos($email, $_arroba) === false){
				$okey = 0;
				$failure = $failure." Email must have an at-sign (@)";
			}				
			if( $okey == 1 ){
				$qryInsert = "INSERT INTO Profile
								(user_id, first_name, last_name, email, headline, summary)
								VALUES (:user_id, :first_name, :last_name, :email, :headline, :summary)
								";
				try{
					$stmt = $link->prepare($qryInsert);
					$stmt->execute(array(
							':user_id' => $user_id,
							':first_name' => $first_name,
							':last_name' => $last_name,
							':email' => $email,
							':headline' => $headline,
							':summary' => $summary)
						);
					$success = "Record edited";
					$_SESSION['success'] = $success;
					
					header("Location: index.php");
					return;					
				}catch(Exception $ex){
					echo '<h3>There was an error, please contact support</h3>';
					error_log("add.php, SQL error= ".$ex->getMessage());
					return;
				}
			}
			$failure = 'hey!!';
			$_SESSION['error'] = $failure;
			header("Location: edit.php?profile_id=" . $_POST['profile_id']);
			return;				
			
		}
		
		if ( isset($_POST['cancel'])  ) {		
			header("Location: index.php");
			return;
		}
		
	}
		
		
		
	
?>
<!DOCTYPE html>
<html>
<head>
<?php require_once "bootstrap.php"; ?>
<title>Juan Munoz's Login Page</title>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>
<body>
<div class="container">
	<h2>Eding Profile for <? echo $name ?></h2>
	<?
		echo '<br>$okey: '.$okey.'<br>';
		if( isset($_SESSION['error']) ){
			echo '<p style="color: red;">'.htmlentities($_SESSION['error'])."</p>\n";
			unset($_SESSION['error']);
		}

		if( isset($_SESSION['first_name']) ){
			$first_name = $_SESSION['first_name'] ;
			unset($_SESSION['first_name']);
		}
		
		if( isset($_SESSION['last_name']) ){
			$last_name = $_SESSION['last_name'] ;
			unset($_SESSION['last_name']);
		}
		if( isset($_SESSION['email']) ){
			$email = $_SESSION['email'] ;
			unset($_SESSION['email']);
		}
		if( isset($_SESSION['headline']) ){
			$headline = $_SESSION['headline'] ;
			unset($_SESSION['headline']);
		}
		if( isset($_SESSION['summary']) ){
			$summary = $_SESSION['summary'] ;
			unset($_SESSION['summary']);
		}	
	?>	
		<div>
		<form method="post">
			<div class="form-row">
				<div class="col">
					<br>
					<label>First Name</label><br>
					<input type="text" class="form-control col-sm-4" name="first_name" id="first_name" value="<? echo $first_name ?>">
				</div>
			</div>
			<div class="form-row">
				<div class="col">
					<br>
					<label>Last Name</label><br>
					<input type="text" class="form-control col-sm-4" name="last_name" id="last_name" value="<? echo $last_name ?>">
				</div>
			</div>
			<div class="form-row">
				<div class="col">
					<br>
					<label>Email</label><br>
					<input type="text" class="form-control col-sm-4" name="email" id="email" value="<? echo $email ?>">
				</div>
			</div>
			<div class="form-row">
				<div class="col">
					<br>
					<label>Headline</label><br>
					<input type="text" class="form-control col-sm-4" name="headline" id="headline" value="<? echo $headline ?>">
				</div>
			</div>
			<div class="form-row">
				<div class="col">
					<br>
					<label>Summary</label><br>
					<input type="text" class="form-control col-sm-4" name="summary" id="summary" value="<? echo $summary ?>">
				</div>
			</div>			
			<div class="form-row">
				<div class="col">
					<br>
					<input type="submit" class="btn btn-success" name="Edit" value="Edit">&nbsp; &nbsp;
					<input type="submit" class="btn btn-primary" name="cancel" value="Cancel">
				</div>
			</div>			
		</form>
		</div>
		<!-- <a href="add.php">Add New </a> | <a href="logout.php"> Logout</a> -->
</div>
</body>
</html>
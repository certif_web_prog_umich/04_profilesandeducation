<?
	require_once('pdo.php');
	session_start();
	if( $_SESSION['name']  == '')
		die('Not logged in');
	else{
		if( $_GET['auto_id'] !== '' ){
			unset($_SESSION['success']);
			unset($_SESSION['error']);			
			$auto_id = $_GET['auto_id'];
			unset($_SESSION['auto_id']);
			$_SESSION['auto_id'] = $auto_id;
			try{
				
				$qry="SELECT * FROM autos WHERE auto_id=:auto_id";
				$stmt = $link->prepare($qry);
				$stmt->execute(array(
					':auto_id' => $auto_id)
					);		
				
				$row = $stmt->fetch(PDO::FETCH_ASSOC);
				
				$make = htmlentities($row['make']);
				$model = htmlentities($row['model']);
				$year = $row['year'];
				$mileage = $row['mileage'];
				
			}catch(Exception $ex){
				echo '<h3>There was an error, please contact support</h3>';
				echo '<a href="logout.php"><button>Return</button></a>';
				error_log("edit.php: , SQL error= ".$ex->getMessage());
				return;
			}
		}else{
			$_SESSION['error'] = "There was an error. Please contact support";
			header("Location: index.php");
			return;
		}
		
		if ( isset($_POST['delete'])  ) {
			$auto_id = $_SESSION['auto_id'];
			unset($_SESSION['auto_id']);
			
			$qry = "DELETE FROM autos WHERE auto_id=:auto_id";
			try{
				$stmt = $link->prepare($qry);
				$stmt->execute(array(
						':auto_id' => $auto_id)
					);
				$success = "Record deleted";
				$_SESSION['success'] = $success;
				
				header("Location: index.php");
				return;					
			}catch(Exception $ex){
				echo '<h3>There was an error, please contact support</h3>';
				error_log("edit.php, SQL error= ".$ex->getMessage());
				return;
			}

		}
		if ( isset($_POST['cancel'])  ) {		
			header("Location: index.php");
			return;
		}
	}
?>
<!DOCTYPE html>
<html>
	<head>
	<?php require_once "bootstrap.php"; ?>
		<title>Juan Munoz's Login Page</title>
		<style>
			table, th, td {
			  border: 1px solid black;
			}
		</style>
		<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
	</head>
	<body>
	<div class="container">
		<?
			if( isset($_SESSION['error']) ){
				echo '<p style="color: red;">'.htmlentities($_SESSION['error'])."</p>\n";
				unset($_SESSION['error']);
			}		
		?>
		<form method="post">
			<div class="form-row">
				<div class="col">
					<br>
					<label><? echo 'Confirm delete '.$make.' '.$model.' '.$year ?></label><br>
					<br>
					<input type="submit" class="btn btn-danger" name="delete" value="Delete">&nbsp; &nbsp;
					<input type="submit" class="btn btn-primary" name="cancel" value="Cancel">					
				</div>
			</div>

			<div class="form-row">
				<div class="col">

				</div>
			</div>			
		</form>
	</div>
	</body>
</html>